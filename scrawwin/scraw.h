/* Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/** \file
 *  \brief Main scraw header
 */

#ifndef _SCRAW_H_
#define _SCRAW_H_
#define _ALLOW_KEYWORD_MACROS

#include "scraw.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef SCRAWWIN_EXPORTS
#define SCRAW_API __declspec(dllexport)
#else
#define SCRAW_API __declspec(dllimport)
#endif // SCRAWWIN_EXPORTS


/*! \ingroup scraw_misc
 *  Result of most function
 */
enum scraw_result {
    /*! Success */
    SCRAW_RESULT_SUCCESS = 0,

    /*! An unspecified error occurred. */
    SCRAW_RESULT_ERROR_UNKNOWN = -1,

    /*! The controller has been disconnected or turned off. */
    SCRAW_RESULT_ERROR_DISCONNECTED = -2,

    /*! The request was not support by the controller. */
    SCRAW_RESULT_NOT_SUPPORTED = -4,
};

/*! \ingroup scraw_ctx
 *  Indicates a global event on the \ref scraw_context_t.
 */
enum scraw_event {
    /*! A new controller is available. */
    SCRAW_EVENT_CONTROLLER_GAINED = 0,

    /*! A controller was disconnected. */
    SCRAW_EVENT_CONTROLLER_LOST = 1,
};

/*! \ingroup scraw_ctx
 *  Opaque type representing a scraw session
 */
typedef struct scraw_context scraw_context_t;

/*! \ingroup scraw_ctx
 *  Callback type for global asynchronous events on a \ref scraw_context_t
 */
typedef void (*scraw_event_cb_t)(scraw_context_t* ctx, int event, void* data, void* user_data);

/*! \ingroup scraw_ctrl
 *  Type of a controller (wired or wireless)
 */
enum scraw_controller_type {
    /*! Wired controller */
    SCRAW_CONTROLLER_TYPE_WIRED = 0,

    /*! Wireless controller */
    SCRAW_CONTROLLER_TYPE_WIRELESS = 1,
};

/*! \ingroup scraw_ctrl
 *  Indicates on which side (left or right) feedback is generated.
 */
enum scraw_feedback_side {
    /*! Generate feedback on the right side of the controller. */
    SCRAW_FEEDBACK_SIDE_RIGHT = 0x00,

    /*! Generate feedback on the left side of the controller. */
    SCRAW_FEEDBACK_SIDE_LEFT = 0x01,
};

/*! \ingroup scraw_ctrl
 *  Indicates the an audio tune.
 *
 *  These tune are used when setting the audio indices with
 *  \ref scraw_controller_set_audio_indices(). The names were taken from the
 *  Steam client.
 */
enum scraw_audio_type {
    /*! Warm And Happy */
    SCRAW_AUDIO_TYPE_WARM_AND_HAPPY = 0x00,

    /*! Invader */
    SCRAW_AUDIO_TYPE_INVADER = 0x01,

    /*! Controller Confirmed */
    SCRAW_AUDIO_TYPE_CONTROLLER_CONFIRMED = 0x02,

    /*! Victory */
    SCRAW_AUDIO_TYPE_VICTORY = 0x03,

    /*! Rise And Shine */
    SCRAW_AUDIO_TYPE_RISE_AND_SHINE = 0x04,

    /*! Shorty */
    SCRAW_AUDIO_TYPE_SHORTY = 0x05,

    /*! Warm Boot */
    SCRAW_AUDIO_TYPE_WARM_BOOT = 0x06,

    /*! Next Level */
    SCRAW_AUDIO_TYPE_NEXT_LEVEL = 0x07,

    /*! Shake It Off */
    SCRAW_AUDIO_TYPE_SHAKE_IT_OFF = 0x08,

    /*! Access Denies */
    SCRAW_AUDIO_TYPE_ACCESS_DENIED = 0x09,

    /*! Deactivate */
    SCRAW_AUDIO_TYPE_DEACTIVATE = 0x0a,

    /*! Discovery */
    SCRAW_AUDIO_TYPE_DISCOVERY = 0x0b,

    /*! Triumph */
    SCRAW_AUDIO_TYPE_TRIUMPH = 0x0c,

    /*! The Mann */
    SCRAW_AUDIO_TYPE_THE_MANN = 0x0d,

    /*! Disabled.
     *
     *  For indices 0 (turn on) and 1 (turn off), this will enable the default
     *  tunes. However, they can not be played with
     *  \ref scraw_controller_play_audio().
     */
    SCRAW_AUDIO_TYPE_NONE = 0xff,
};

/*! \ingroup scraw_ctrl
 *  Indicates the component to calibrate.
 *
 *  Use with \ref scraw_controller_calibrate().
 */
enum scraw_calibrate_component {
    /*! Calibrates trackpads. */
    SCRAW_CALIBRATE_TRACKPAD = 0,

    /*! Calibrates joystick. */
    SCRAW_CALIBRATE_JOYSTICK = 1,

    /*! Calibrates IMU (inertial measurement unit). */
    SCRAW_CALIBRATE_IMU = 2,
};

/*! \ingroup scraw_ctrl
 *  Discrete controller buttons
 */
enum scraw_button {
    /*! Right gripper */
    SCRAW_BTN_RIGHT_GRIPPER = 0x00000001,

    /*! Left trackpad
     *
     *  This indices a full physical press of the left trackpad.
     */
    SCRAW_BTN_LEFT_TP = 0x00000002,

    /*! Right trackpad
     *
     *  This indices a full physical press of the right trackpad.
     */
    SCRAW_BTN_RIGHT_TP = 0x00000004,

    /*! Left trackpad touch
     *
     *  This indices that left trackpad is touched.
     */
    SCRAW_BTN_LEFT_TP_TOUCH = 0x00000008,

    /*! Right trackpad touch
     *
     *  This indices that right trackpad is touched.
     */
    SCRAW_BTN_RIGHT_TP_TOUCH = 0x00000010,

    /*! Joystick */
    SCRAW_BTN_JOYSTICK = 0x00000040,

    /*! Left trackpad up
     *
     *  This indices a full physical press in the upper section of the left
     *  trackpad.
     */
    SCRAW_BTN_LEFT_TP_UP = 0x00000100,

    /*! Left trackpad up
     *
     *  This indices a full physical press in the right section of the left
     *  trackpad.
     */
    SCRAW_BTN_LEFT_TP_RIGHT = 0x00000200,

    /*! Left trackpad left
     *
     *  This indices a full physical press in the left section of the left
     *  trackpad.
     */
    SCRAW_BTN_LEFT_TP_LEFT = 0x00000400,

    /*! Left trackpad left
     *
     *  This indices a full physical press in the lower section of the left
     *  trackpad.
     */
    SCRAW_BTN_LEFT_TP_DOWN = 0x00000800,

    /*! Select */
    SCRAW_BTN_SELECT = 0x00001000,

    /*! Steam button */
    SCRAW_BTN_STEAM = 0x00002000,

    /*! Start */
    SCRAW_BTN_START = 0x00004000,

    /*! Left Gripper */
    SCRAW_BTN_LEFT_GRIPPER= 0x00008000,

    /*! Right trigger
     *
     *  This indicates that the right trigger has been pulled down completely,
     *  past the small barrier at the end.
     */
    SCRAW_BTN_RIGHT_TRIGGER = 0x00010000,

    /*! Left trigger
     *
     *  This indicates that the left trigger has been pulled down completely,
     *  past the small barrier at the end.
     */
    SCRAW_BTN_LEFT_TRIGGER = 0x00020000,

    /*! Right shoulder button */
    SCRAW_BTN_RIGHT_SHOULDER = 0x00040000,

    /*! Left shoulder button */
    SCRAW_BTN_LEFT_SHOULDER = 0x00080000,

    /*! Y button */
    SCRAW_BTN_Y = 0x00100000,

    /*! B button */
    SCRAW_BTN_B = 0x00200000,

    /*! X button */
    SCRAW_BTN_X = 0x00400000,

    /*! A button */
    SCRAW_BTN_A = 0x00800000,
};

/*! \ingroup scraw_ctrl
 *  Opaque type representing a controller
 */
typedef struct scraw_controller scraw_controller_t;

/*! \ingroup scraw_ctrl
 *  Complete state of the controller
 *
 *  This includes all discrete button, axes and trackpads.
 */
typedef struct {
    /*! Size of this struct
     *
     *  This is used for backward-compatibility. Has to be set when querying
     *  controller state with \ref scraw_controller_get_state() and will be set
     *  by scraw when using asynchronous callbacks.
     */
    size_t size;

    /*! Bit-field of all discrete buttons.
     *
     *  See \ref scraw_button.
     */
    uint32_t buttons;

    /*! Left trigger
     *
     *  Values range from 0 to 32767.
     *
     *  The wired controller provides 15-bit resolution for this field. The
     *  wireless controller provides only 8-bit. The actual value will always
     *  be linearly scaled to the range mentioned above.
     */
    int16_t left_trigger;

    /*! Right trigger
     *
     *  Values range from 0 to 32767.
     *
     *  The wired controller provides 15-bit resolution for this field. The
     *  wireless controller provides only 8-bit. The actual value will always
     *  be linearly scaled to the range mentioned above.
     */
    int16_t right_trigger;

    /*! Left trackpad horizontal position
     *
     *  Values take on the full range of int16_t.
     */
    int16_t left_trackpad_x;

    /*! Left trackpad vertical position
     *
     *  Values take on the full range of int16_t.
     */
    int16_t left_trackpad_y;

    /*! Right trackpad horizontal position
     *
     *  Values take on the full range of int16_t.
     */
    int16_t right_trackpad_x;

    /*! Right trackpad vertical position
     *
     *  Values take on the full range of int16_t.
     */
    int16_t right_trackpad_y;

    /*! Joystick horizontal position
     *
     *  Values take on the full range of int16_t.
     */
    int16_t joystick_x;

    /*! Joystick vertical position
     *
     *  Values take on the full range of int16_t.
     */
    int16_t joystick_y;

    /*! Delta pitch of the IMU
     *
     *  Values take on the full range of int16_t.
     */
    int16_t d_imu_pitch;

    /*! Delta roll of the IMU
     *
     *  Values take on the full range of int16_t.
     */
    int16_t d_imu_roll;

    /*! Delta yaw of the IMU
     *
     *  Values take on the full range of int16_t.
     */
    int16_t d_imu_yaw;
} scraw_controller_state_t;

/*! \ingroup scraw_ctrl
 *  Controller configuration
 *
 *  This struct corresponds to one specific control packet. So far, only two
 *  fields have been identified. It might be extended in the future.
 */
typedef struct {
    /*! Size of this struct
     *
     *  This is used for backward-compatibility. Has to be set when configuring
     *  the controller with \ref scraw_controller_configure().
     */
    size_t size;

    /*! Idle timeout in seconds after which the controller turns off */
    uint16_t idle_timeout;

    /*! Whether the IMU is enabled or not */
    bool imu;
} scraw_controller_config_t;

/*! \ingroup scraw_ctrl
 *  Various information about a controller
 */
typedef struct {
    /*! Size of this struct
     *
     *  This is used for backward-compatibility. Has to be set when querying the
     *  information.
     */
    size_t size;

    /*! Type of the controller
     *
     *  See \ref scraw_controller_type.
     */
    int type;

    /*! Product ID
     *
     *  This is not necessarily the USB product id. At the moment the always
     *  contains 0x1102 (4354).
     */
    uint16_t product_id;

    /*! Bootloader timestamp
     *
     *  Measured as Unix time.
     */
    uint32_t bootloader_ts;

    /*! Firmware timestamp
     *
     *  Measured as Unix time.
     */
    uint32_t firmware_ts;

    /*! Radio timestamp
     *
     *  Measured as Unix time.
     */
    uint32_t radio_ts;

    /*! Null-terminated serial number */
    char serial_number[11];

    /*! Receiver firmware timestamp
     *
     *  Only valid for wireless controllers. Measured as Unix time.
     */
    uint32_t receiver_firmware_ts;

    /*! Null-terminated receiver serial number
     *
     *  Only valid for wireless controllers.
     */
    char receiver_serial_number[11];
} scraw_controller_info_t;

/*! \ingroup scraw_ctrl
 *  Callback type for asynchronous state change events
 */
typedef void (*scraw_state_change_cb_t)(scraw_controller_t* ctrl, const scraw_controller_state_t* state);

/*! \ingroup scraw_ctrl
 *  Callback type for asynchronous controller destruction events
 */
typedef void (*scraw_controller_destroy_cb_t)(scraw_controller_t* ctrl);

#ifdef  __cplusplus
extern "C" {
#endif

/*! Runtime major version */
SCRAW_API int scraw_version_major();

/*! Runtime minor version */
SCRAW_API int scraw_version_minor();

/*! Runtime patch version */
SCRAW_API int scraw_version_patch();

/*! \ingroup scraw_ctx
 *  Create a scraw context.
 *
 *  Creates and initialized a new context. If a callback is passed, it will be
 *  called immediately with \ref SCRAW_EVENT_CONTROLLER_GAINED for all
 *  available controllers.
 *
 *  \param ctx New context if this function returns \ref SCRAW_RESULT_SUCCESS
 *  \param cb Optional callback for global events. Can be NULL.
 *  \param user_data Pointer that is passed to the global event handler
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_context_create(scraw_context_t** ctx, scraw_event_cb_t cb, void* user_data);

/*! \ingroup scraw_ctx
 *  Destroy context.
 *
 *  Call this function only after all references to controllers have been
 *  released. The event callback will be called with
 *  \ref SCRAW_EVENT_CONTROLLER_LOST for every controller before this function
 *  exists. Instead of releasing all controllers beforehand, it is okay to
 *  release them during this final event.
 *
 *  \param ctx Context
 *  \return \ref scraw_result
 */
SCRAW_API void scraw_context_destroy(scraw_context_t* ctx);

/*! \ingroup scraw_ctx
 *  Perform event handling.
 *
 *  This function has to be called periodically. It is allowed to call this
 *  function from multiple threads at any time, except from inside callbacks.
 *
 *  \param ctx Context
 *  \param timeout_ms Timeout in milliseconds. 0 for non-blocking mode.
 *  \return \ref scraw_result
 */
SCRAW_API void scraw_context_handle_events(scraw_context_t* ctx, long timeout_ms);

/*! \ingroup scraw_ctx
 *  Get a List of all available controllers.
 *
 *  The reference count of all controller in the list will be increased.
 *
 *  \param ctx Context
 *  \param ctrls List of controllers. Has to be freed with
 *               \ref scraw_context_free_controller_list().
 *  \param num_ctrls Number of controllers in the list
 *  \return \ref scraw_result
 */
SCRAW_API void scraw_context_list_controllers(scraw_context_t* ctx, scraw_controller_t*** ctrls, int* num_ctrls);

/*! \ingroup scraw_ctx
 *  Free controller list.
 *
 *  The reference count of all controllers in the list will be decreased. You
 *  have to retain a controller before calling this function if you plan on
 *  using it.
 *
 *  \param ctrls List of controllers from \ref scraw_context_list_controllers()
 *  \return \ref scraw_result
 */
SCRAW_API void scraw_context_free_controller_list(scraw_controller_t** ctrls);

/*! \ingroup scraw_ctrl
 *  Increase controller reference counter.
 *
 *  \param ctrl Controller
 *  \return \ref scraw_result
 */
SCRAW_API void scraw_controller_retain(scraw_controller_t* ctrl);

/*! \ingroup scraw_ctrl
 *  Decrease controller reference counter.
 *
 *  The controller object will be destroyed once the reference counter reaches 0.
 *
 *  \param ctrl Controller
 *  \return \ref scraw_result
 */
SCRAW_API void scraw_controller_release(scraw_controller_t* ctrl);

/*! \ingroup scraw_ctrl
 *  Set the callback for controller destroy events.
 *
 *  The callback will be called when the internal reference counter reaches zero.
 *
 *  \param ctrl Controller
 *  \param cb Callback
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_on_destroy(scraw_controller_t* ctrl, scraw_controller_destroy_cb_t cb);

/*! \ingroup scraw_ctrl
 *  Set the user data pointer.
 *
 *  \param ctrl Controller
 *  \param user_data User data
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_set_user_data(scraw_controller_t* ctrl, void* user_data);

/*! \ingroup scraw_ctrl
 *  Get the user data pointer.
 *
 *  This function can be called even after the controller has been disconnected.
 *
 *  \param ctrl Controller
 *  \param user_data Will be set to the user data.
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_get_user_data(scraw_controller_t* ctrl, void** user_data);

/*! \ingroup scraw_ctrl
 *  Query whether the controller is connected or not.
 *
 *  This function can be called after a controller has been disconnected, but
 *  only if the reference counter is still above 0.
 *
 *  \param ctrl Controller
 *  \return \ref scraw_result
 *          (\ref SCRAW_RESULT_SUCCESS if it is connected;
 *          SCRAW_RESULT_ERROR_DISCONNECTED otherwise)
 */
SCRAW_API int scraw_controller_is_available(scraw_controller_t* ctrl);

/*! \ingroup scraw_ctrl
 *  Query the controller type.
 *
 *  This function can be called after a controller has been disconnected, but
 *  only if the reference counter is still above 0.
 *
 *  \param ctrl Controller
 *  \param type See \ref scraw_controller_type.
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_get_type(scraw_controller_t* ctrl, int* type);

/*! \ingroup scraw_ctrl
 *  Query information about a controller.
 *
 *  This function can cause a noticeable delay of several hundred milliseconds.
 *
 *  \param ctrl Controller
 *  \param info Will be filled with controller information
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_get_info(scraw_controller_t* ctrl, scraw_controller_info_t* info);

/*! \ingroup scraw_ctrl
 *  Set the callback for state change events.
 *
 *  The callback will be called every time the state changes. This also clears
 *  the internal `changed` flag, which means subsequent calls to scraw_get_state
 *  will not update the state if it hasn't changed since the callback was called.
 *
 *  \param ctrl Controller
 *  \param cb Callback
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_on_state_change(scraw_controller_t* ctrl, scraw_state_change_cb_t cb);

/*! \ingroup scraw_ctrl
 *  Get the current state of a controller.
 *
 *  \p state will only be overridden if and only if \p changed is not NULL and
 *  the actual state of the controller has changed and not been queried
 *  (asynchronously or synchronously) since then. In other words: Set \p changed
 *  to NULL if you what the current state independently of whether it has
 *  changed or not.
 *
 *  \param ctrl Controller
 *  \param state Will be filled with the current state
 *  \param changed Will be set to true if the state has changed since it was
 *         last queried. Can be NULL.
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_get_state(scraw_controller_t* ctrl, scraw_controller_state_t* state, bool* changed);

/*! \ingroup scraw_ctrl
 *  Configure a controller.
 *
 *  Calling this function will also disable lizard mode on all inputs except the
 *  discrete buttons.
 *
 *  \param ctrl Controller
 *  \param config Configuration
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_configure(scraw_controller_t* ctrl, scraw_controller_config_t* config);

/*! \ingroup scraw_ctrl
 *  Enable or disable lizard mode for all discrete buttons.
 *
 *  \param ctrl Controller
 *  \param enable Whether to enable or disable lizard mode.
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_lizard_buttons(scraw_controller_t* ctrl, bool enable);

/*! \ingroup scraw_ctrl
 *  Enable lizard mode for all analog inputs.
 *
 *  This includes the joystick, left and right trigger and both trackpads.
 *
 *  \param ctrl Controller
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_enable_lizard_analog(scraw_controller_t* ctrl);

/*! \ingroup scraw_ctrl
 *  Produce haptic feedback.
 *
 *  \param ctrl Controller
 *  \param side Left or right. See \ref scraw_feedback_side.
 *  \param amplitude Amplitude
 *  \param period Period
 *  \param count Count
 *  \return \ref scraw_result
 *
 *  \remarks
 *  The official Steam client uses this to play small sounds as well.
 *  E.g.: amplitude=500 period=500 count=40 will produce a short beep.
 */
SCRAW_API int scraw_controller_feedback(scraw_controller_t* ctrl, uint8_t side, uint16_t amplitude, uint16_t period, uint16_t count);

/*! \ingroup scraw_ctrl
 *  Control the LED brightness
 *
 *  \param ctrl Controller
 *  \param percent Percentage of the maximum brightness. 0 turns the LED off.
 *  \return \ref scraw_result
 *
 *  \remarks
 *  This has no effect while lizard mode is active.
 */
SCRAW_API int scraw_controller_set_led_brightness(scraw_controller_t* ctrl, uint8_t percent);

/*! \ingroup scraw_ctrl
 *  Associates indices with audio tunes.
 *
 *  Each index 0-15 will be associated an audio tune. The available
 *  tunes are defined in \ref scraw_audio_type. The indices are
 *  persistent between controller power-cycles.
 *
 *  The first two indices correspond to the tune played when turning
 *  the controller on (index 0) and off (index 1).
 *
 *  \param ctrl Controller
 *  \param indices An 16-elements array of \ref scraw_audio_type values
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_set_audio_indices(scraw_controller_t* ctrl, uint8_t* indices);

/*! \ingroup scraw_ctrl
 *  Plays an audio tune identified by an index.
 *
 *  Set the valid indices beforehand with
 *  \ref scraw_controller_set_audio_indices().
 *
 *  \param ctrl Controller
 *  \param index Index of the tune
 *  \return \ref scraw_result
 */
SCRAW_API int scraw_controller_play_audio(scraw_controller_t* ctrl, uint8_t index);

/*! \ingroup scraw_ctrl
 *  Turns off a wireless controller.
 *
 *  \param ctrl Controller
 *  \return \ref scraw_result
 *          (\ref SCRAW_RESULT_NOT_SUPPORTED for non-wireless controller)
 */
SCRAW_API int scraw_controller_turn_off(scraw_controller_t* ctrl);

/*! \ingroup scraw_ctrl
 *  Calibrates a component of the controller.
 *
 *  \param ctrl Controller
 *  \param component The component to calibrate. See \ref scraw_calibrate_component.
 *  \return \ref scraw_result
 *
 *  \remarks
 *  The official Steam client waits several seconds after calibrating IMU.
 */
SCRAW_API int scraw_controller_calibrate(scraw_controller_t* ctrl, int component);

#ifdef  __cplusplus
}
#endif

#endif /* _SCRAW_H_ */
