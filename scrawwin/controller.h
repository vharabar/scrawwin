/* Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef _SCRAW_CONTROLLER_H_
#define _SCRAW_CONTROLLER_H_

#include "scraw.h"
#include "packets.h"
#include <stdbool.h>
#include <stdint.h>
#include <thr\threads.h>
#include <libusb-1.0\libusb.h>

typedef struct device device_t;

typedef struct {
    scraw_controller_state_t state;
    bool changed;
} state_t;

struct scraw_controller {
    int type;
    mtx_t mutex;
	mtx_t rw_mutex;
    bool connected;
    bool hotplug_status;
    uint32_t ref_count;
    int io_wait;
    device_t* dev;
    int usb_interface;
    int usb_int_endpoint;
    struct libusb_transfer* transfer;
    int transfer_destroyed;
    packet int_packet;
    uint32_t seq_number;
    int cur_state;
    state_t state[2];
    scraw_state_change_cb_t state_cb;
    void* user_data;
    scraw_controller_destroy_cb_t destroy_cb;
};

int controller_create(scraw_controller_t** ctrl, device_t* dev, int interface);
void controller_destroy(scraw_controller_t* ctrl);
void controller_raise_state_change(scraw_controller_t* ctrl);

#endif /* _SCRAW_CONTROLLER_H_ */
